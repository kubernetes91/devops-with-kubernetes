## Exercise 2.02: Project v1.0

Create a new container for the backend of the todo application.

You can use graphql or other solutions if you want.

Use ingress routing to enable access to the backend.

Create a POST /todos endpoint and a GET /todos endpoint in the new service where we can post a new todo and get all of the todos. You can also move the image logic to the new service if it requires backend logic.

The todos can be saved into memory, we'll add database later.

Frontend already has an input field. Connect it into our backend so that inputting data and pressing send will add a new todo into the list.

## Deployment

```bash
# Run a kubernetes cluster forwarding host ports 8081 (frontend), and 8091 (backend) to a load-balancer port 80
k3d cluster create -p 8081:80@loadbalancer -p 8091:80@loadbalancer --agents 2

# For the PersistentVolume to work we need to create the local path in the node we are binding it to.
# Since our k3d cluster runs via docker let's create a directory at
# `/tmp/kube`
# in the `k3d-k3s-default-agent-0` container:
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube

# Deploy persistent volume and persistent volume claim:
kubectl apply -f manifests/volume/

# And deploy the app:
kubectl apply -f manifests/
```

App is available on port 8081: http://localhost:8081/.
You also can send GET requests to the backend directly http://localhost:8081/todos, http://localhost:8081/daily_image.
