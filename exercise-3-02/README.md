### Exercise 3.02: Back to Ingress

Deploy the "Log output" and "Ping-pong" applications into GKE and expose it with Ingress.

"Ping-pong" will have to respond from /pingpong path. This may require you to rewrite parts of the code.

### Deployment to Google Kubernetes Engine

Encrypt the secrete and de-crypt it for deployment in production environment.
Create a namespace, wait until it would be ready, and deploy other services:

```shell
kubectl apply -f manifests/namespace.yml
```

```shell
kubectl apply -f manifests/ping-pong/
```

```shell
kubectl apply -f manifests/logoutput/
```

```shell
kubectl apply -f manifests/ingress.yml
```

App is available on port 80, on paths: `/pingpong`, `/count`, and `status`.
