### Exercise 2.09: Daily todos
Create a CronJob that generates a new todo every day to remind you to do 'Read < URL >'.

Where < URL > is a wikipedia article that was decided by the job randomly. It does not have to be a hyperlink, the user can copy paste the url from the todo.

https://en.wikipedia.org/wiki/Special:Random responds with a redirect to a random wikipedia page so you can ask it to provide a random article for you to read. TIP: Check location header

### Run in Kubernetes

Run k8s cluster, forwarding host's ports 8081 (frontend endpoint), and 8091 (backend endpoint) to load-balancer port 80.

```shell
k3d cluster create -p 8081:80@loadbalancer -p 8091:80@loadbalancer --agents 2
```

For the PersistentVolume to work we need to create the local path in the node we are binding it to.
Since our k3d cluster runs via docker let's create a directory at
`/tmp/kube`
in the `k3d-k3s-default-agent-0` container:

```shell
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube
````

Create namespace:

```shell
kubectl apply -f manifests/namespace.yml
```

Deploy persistent volume and persistent volume claim:

```shell
kubectl apply -f manifests/volume/
```

Create secret:
```shell
kubectl apply -f manifests/secret.yml
```

Deploy postgres:
```shell
kubectl apply -f manifests/postgres.yml

```

Deploy backend:

```shell
kubectl apply -f manifests/java-backend.yml
```

Deploy frontend:
```shell
kubectl apply -f manifests/react-frontend.yml
```

Deploy ingress:
```shell
kubectl apply -f manifests/ingress.yml
```

Run containerised shell script which adds link to a Wiki page to todo list:
```shell
kubectl apply -f manifests/todo-job.yml
```

Deploy a cron job that adds link to a Wiki page to todo list every 5 minutes:
```shell
kubectl apply -f manifests/todo-cronjob.yml
```

App is available on port 8081: http://localhost:8081/
You also can send requests to the backend directly http://localhost:8081/todos , http://localhost:8081/daily_image 
