## Exercise 2.04: Project v1.1

Create a namespace for the project and move everything related to the project to that namespace.

## Deployment

```bash
# Run a kubernetes cluster forwarding host ports 8081 (frontend), and 8091 (backend) to a load-balancer port 80
k3d cluster create -p 8081:80@loadbalancer -p 8091:80@loadbalancer --agents 2

# For the PersistentVolume to work we need to create the local path in the node we are binding it to.
# Since our k3d cluster runs via docker let's create a directory at
# `/tmp/kube`
# in the `k3d-k3s-default-agent-0` container:
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube

# Create namespace at first
kubectl apply -f manifests/namespace.yml

# Deploy persistent volume and persistent volume claim:
kubectl apply -f manifests/volume/

# And deploy the app:
kubectl apply -f manifests/
```

App is available on port 8081: http://localhost:8081/.
You also can send GET requests to the backend directly http://localhost:8081/todos, http://localhost:8081/daily_image.
