## Exercise 1.13: Project v0.7

For the project we'll need to do some coding to start seeing results in the next part.

Add an input field. The input should not take todos that are over 140 characters long.
Add a send button. It does not have to send the todo yet.
Add a list for the existing todos with some hardcoded todos.
Maybe something similar to this:
![image](https://devopswithkubernetes.com/static/ff807ebf379aa4fd08d98d96b03d969c/8ae5a/project-ex-113.webp)

## Deployment

```bash
# Run a kubernetes cluster forwarding host port 8081 to a load-balancer port 80
k3d cluster create -p 8081:80@loadbalancer --agents 2

# Create a folder in the agent-0 container in order to use it as a local storage
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube

# Deploy local volume and volume-claim manifests
kubectl apply -f manifests/volume/

# Deploy app on the cluster
kubectl apply -f manifests/
```

View the app: http://localhost:8081.
