### Exercise 3.01: Pingpong GKE

Deploy ping / pong application into GKE.
In this exercise use a LoadBalancer service to expose the service.

### Run in Google Kubernetes Engine

Run from manifests folder.
(Encrypt the secrete and de-crypt it for deployment in production environment.)
Create a namespace, wait until it would be ready, and deploy other services:

```shell
kubectl apply -f namespace.yml
kubectl wait --for=condition=Ready namespace -l name=devops
kubectl apply -f secret.yml -f postgres.yml -f deployment.yml -f service.yml
```

App is available on port 8081, on paths: `/pingpong` and `/count`.
