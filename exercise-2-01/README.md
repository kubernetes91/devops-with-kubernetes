## Exercise 2.01: Connecting pods

Connect the "Log output" application and "Ping-pong" application.
Instead of sharing data via files use HTTP endpoints to respond with the number of pongs.
Deprecate all the volume between the two applications for the time being.

The output will stay the same:

```bash
2020-03-30T12:15:17.705Z: 8523ecb1-c716-4cb6-a044-b9e83bb98e43.
Ping / Pongs: 3
```

## Deployment

```bash
# Run a kubernetes cluster forwarding host port 8081 to a load-balancer port 80
k3d cluster create -p 8081:80@loadbalancer --agents 2

# Create a folder in the agent-0 container in order to use it as a local storage
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube

# Deploy local volume and volume-claim manifests
kubectl apply -f manifests/volume/

# Deploy pin-pong app on the cluster
kubectl apply -f manifests/ping-pong/

# Deploy log-output app on the cluster
kubectl apply -f manifests/log-output/

# Deploy an ingress for both apps
kubectl apply -f manifests/ingress.yml
```

Ping-pong app: `http://localhost:8081/pingpong`.

Ping-pong app: `http://localhost:8081/count`.

Log-output app: `http://localhost:8081/status`.
