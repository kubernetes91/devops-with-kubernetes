## Exercise 1.11: Persisting data

Let's share data between "Ping-pong" and "Log output" applications using persistent volumes. Create both a PersistentVolume and PersistentVolumeClaim and alter the Deployment to utilize it. As PersistentVolume is often maintained by cluster administrators rather than developers and are not application specific you should keep the definition for that separated.

Save the number of requests to "Ping-pong" application into a file in the volume and output it with the timestamp and hash when sending a request to our "Log output" application. In the end, the two pods should share a persistent volume between the two applications. So the browser should display the following when accessing the "Log output" application:

2020-03-30T12:15:17.705Z: 8523ecb1-c716-4cb6-a044-b9e83bb98e43.
Ping / Pongs: 3

### How to deploy

```bash
# Run a kubernetes cluster forwarding host port 8081 to a load-balancer port 80
k3d cluster create -p 8081:80@loadbalancer --agents 2

# Create a folder in the agent-0 container in order to use it as a local storage
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube

# Deploy local volume and volume-claim manifests
kubectl apply -f manifests/volume

# Deploy pin-pong app on the cluster
kubectl apply -f manifests/ping-pong-manifests/

# Deploy log-output app on the cluster
kubectl apply -f manifests/log-output-manifests/
```

Ping-pong app: `http://localhost:8081/pingpong`.
Log-output app: `http://localhost:8081/status`.
