## Exercise 2.03: Keep them separated
Create a namespace for the applications in the exercises. 
Move the "Log output" and "Ping-pong" to that namespace and use that in the future for all of the exercises. 
You can follow the material in the default namespace.

## Deployment

```bash
# Run a kubernetes cluster forwarding host port 8081 to a load-balancer port 80
k3d cluster create -p 8081:80@loadbalancer --agents 2

# Create a folder in the agent-0 container in order to use it as a local storage
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube

# Deploy a namespace and an ingress for the apps
kubectl apply -f manifests/namespace.yml
kubectl apply -f manifests/ingress.yml

# Deploy local volume and volume-claim manifests
kubectl apply -f manifests/volume/

# Deploy pin-pong app on the cluster
kubectl apply -f manifests/ping-pong/

# Deploy log-output app on the cluster
kubectl apply -f manifests/log-output/

```

Wait until all stuff will be established, getting Bad Gateway response for initialization time.

Ping-pong app: http://localhost:8081/pingpong.

Ping-pong app: http://localhost:8081/count.

Log-output app: http://localhost:8081/status.
