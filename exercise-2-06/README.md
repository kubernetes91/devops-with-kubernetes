## Exercise 2.06: Documentation and ConfigMaps
Use the official Kubernetes documentation for this exercise. 
https://kubernetes.io/docs/concepts/configuration/configmap/ 
and 
https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/ 
should contain everything you need.

Create a ConfigMap for a "dotenv file". 
A file where you define environment variables that are loaded by the application. 
For this use an environment variable "MESSAGE" with value "Hello" to test and print the value. 
The values from ConfigMap can be either saved to a file and read by the application, 
or set as environment variables and used by the application through that. 
Implementation is up to you but the output should look like this:

```bash
Hello
2020-03-30T12:15:17.705Z: 8523ecb1-c716-4cb6-a044-b9e83bb98e43.
Ping / Pongs: 3
```

## Deployment

```bash
# Run a kubernetes cluster forwarding host port 8081 to a load-balancer port 80
k3d cluster create -p 8081:80@loadbalancer --agents 2

# Create a folder in the agent-0 container in order to use it as a local storage
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube

# Deploy a namespace and an ingress for the apps
kubectl apply -f manifests/namespace.yml
kubectl apply -f manifests/ingress.yml

# Deploy local volume and volume-claim manifests
kubectl apply -f manifests/volume/

# Deploy pin-pong app on the cluster
kubectl apply -f manifests/ping-pong/

# Deploy log-output app on the cluster
kubectl apply -f manifests/log-output/

```

Wait until all stuff will be established, getting Bad Gateway response for initialization time.

Ping-pong app: http://localhost:8081/pingpong.

Ping-pong app: http://localhost:8081/count.

Log-output app: http://localhost:8081/status.
