## Exercise 2.07: Stateful applications
Run a postgres database and save the Ping-pong application counter into the database.

The postgres database and Ping-pong application should not be in the same pod. 
A single postgres database is enough and it may disappear with the cluster 
but it should survive even if all pods are taken down.

You should not write the database password in plain text.

## Deployment

#### Run a kubernetes cluster forwarding host port 8081 to a load-balancer port 80

```shell
# Create cluster with default name, if you want to name it change name of the node where volume is created accordingly
# in docker exec command and in the local volume manifest
k3d cluster create -p 8081:80@loadbalancer --agents 2
```

```shell
# create folder on a kubernetes node to use it as a persisten volume
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube
```

#### Encrypt secret

```shell
# encrypt manifests/secret.yml use your own public key for that
# sops --encrypt --age <public_key> secret.yml > secret.enc.yml
sops --encrypt --age age1ssqylrszr8sj6ys8glqhrr7w0zkdve80kyndcsuhu3qsx3zywaxsxph2z0 secret.yml > secret.enc.yml

# you can verify encrypted secret
export SOPS_AGE_KEY_FILE=keys/key.txt
sops --decrypt secret.enc.yml > secret.decrypted.yml
```
#### Run pin-pong app on the cluster

```shell
# create namespace
kubectl apply -f manifests/namespace.yml
```
```shell
# create persistent volume
kubectl apply -f manifests/volume
```
```shell
# create secret
# kubectl apply -f manifests/secret.yml
#
# or decrypt secret at first
export SOPS_AGE_KEY_FILE=keys/key.txt
sops --decrypt manifests/secret.enc.yml | kubectl apply -f -
```
```shell
# lunch postgres and wait until it starts
kubectl apply -f manifests/postgres.yml
kubectl wait --for=condition=Ready pod -l app=ping-pong-app -n devops-exercise
```
```shell
# deploy ping-pong
kubectl apply -f manifests/deployment.yml -f manifests/service.yml -f manifests/ingress.yml
```

App is available on port 8081: http://localhost:8081/pingpong, http://localhost:8081/count
