#### Exercise 2.08: Project v1.2

Create a database and save the todos there.

Use Secrets and/or ConfigMaps to have the backend access the database.

#### Run in Kubernetes

Run k8s cluster, forwarding host's ports 8081 (frontend endpoint), and 8091 (backend endpoint) to load-balancer port 80.

```shell
k3d cluster create -p 8081:80@loadbalancer -p 8091:80@loadbalancer --agents 2
```

We use a persistent volume to cache daily image.
For the PersistentVolume to work we need to create the local path in the node we are binding it to.
Since our k3d cluster runs via docker let's create a directory at
`/tmp/kube`
in the `k3d-k3s-default-agent-0` container:

```shell
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube
````

Create namespace:

```shell
kubectl apply -f manifests/namespace.yml
```

Deploy persistent volume and persistent volume claim:

```shell
kubectl apply -f manifests/volume/
```

Create secret:

```shell
kubectl apply -f manifests/secret.yml
```

In real life we do not commit secret to a VCS, but commit encrypted secret,
and decrypt it for deployment.
Here we use the [SOPS](https://github.com/mozilla/sops) and the [age](https://github.com/FiloSottile/age) utility to encript values stored in yaml files.
`SOPS` handles the yaml structure and `age` does actual encryption. 
Encrypt secret with a public key (stored in the `keys/key.txt` file):

```shell
sops --encrypt --age age1ssqylrszr8sj6ys8glqhrr7w0zkdve80kyndcsuhu3qsx3zywaxsxph2z0 manifests/secret.yml > manifests/secret.enc.yml
```

Verify encrypted secret:
```shell
export SOPS_AGE_KEY_FILE=keys/key.txt
sops --decrypt manifests/secret.enc.yml > manifests/secret.decrypted.yml
```

Deploy secret:
```shell
sops --decrypt manifests/secret.enc.yml | kubectl apply -f -
```

Deploy postgres:

```shell
kubectl apply -f manifests/postgres.yml
```

Deploy backend:

```shell
kubectl apply -f manifests/java-backend.yml
```

Deploy frontend:

```shell
kubectl apply -f manifests/react-frontend.yml
```

Deploy ingress:

```shell
kubectl apply -f manifests/ingress.yml
```

App is available on port 8081: <http://localhost:8081/>
You also can send requests to the backend directly <http://localhost:8081/todos> , <http://localhost:8081/daily_image>
